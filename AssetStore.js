import Sprite from './Sprite';
import EventTargetClass from './EventTarget';

const extensionMatch = /\.([^.]+)$/;
const extensionMimeTypes = {
  'wav': 'audio/wav',
  'mp3': 'audio/mpeg',
  'ogg': 'audio/ogg; codecs=vorbis',
};

function urlToModule(url) {
  if (url.startsWith('.')) {
    return url.replace(/\.js$/, '');
  } else {
    return './' + url.replace(/\.js$/, '');
  }
}

function makePrefabClass(config) {
  if (typeof config === 'function') {
    // If the config is a callable, assume it's a constructor.
    return config;
  }
  const prefab = Sprite.prepareConfig(config, false);
  return class extends Sprite {
    constructor(origin) {
      super(prefab, origin);
    }
  };
}

export class AssetStore extends EventTargetClass {
  constructor(assetlist) {
    super();
    this.images = {};
    this.prefabs = {};
    this.data = {};
    this.modules = {};
    this.sounds = {};
    this.pendingKeys = new Set();
    this.loadComplete = 0;
    if (assetlist) {
      this.load(assetlist);
    }
  }

  get loadProgress() {
    return this.loadComplete / (this.loadComplete + this.pendingKeys.size);
  }

  _startLoading(key, collection) {
    const combined_key = collection ? collection + '_' + key : key;
    this.pendingKeys.add(combined_key);
    this.dispatchEvent('loadprogress', this.loadProgress);
  }

  _bindDoneLoading(key, collection) {
    return (value) => {
      this.pendingKeys.delete(collection ? collection + '_' + key : key);
      this.loadComplete++;
      this.dispatchEvent('loadprogress', this.loadProgress);
      if (collection) {
        this[collection][key] = value;
      }
      if (this.pendingKeys.size === 0) {
        // If nothing remains to be loaded, wait for the event loop and then fire the loadcomplete event.
        setTimeout(() => this.dispatchEvent('loadcomplete', this), 0);
      }
      return value;
    };
  }

  loadSoundAssets(assets) {
    return Promise.all(Object.keys(assets || {}).map(key => this.loadSoundAsset(key, assets[key]))).then(() => this);
  }

  loadSoundAsset(key, urls) {
    if (this.sounds[key]) {
      return Promise.resolve(this.sounds[key]);
    }
    this._startLoading(key, 'sounds');
    return new Promise(resolve => {
      const element = document.createElement('AUDIO');
      element.addEventListener('canplay', () => resolve(element), { once: true });
      element.addEventListener('error', () => resolve(element), { once: true });
      if (Array.isArray(urls)) {
        for (const url of urls) {
          const source = document.createElement('SOURCE');
          source.src = url;
          const extension = (extensionMatch.exec(url) || [])[1];
          const mimeType = extensionMimeTypes[extension];
          if (mimeType) {
            source.type = mimeType;
          }
          element.appendChild(source);
        }
      } else if (typeof urls === 'object') {
        for (const mimeType of Object.keys(urls)) {
          const source = document.createElement('SOURCE');
          source.src = urls[mimeType];
          source.type = mimeType;
          element.appendChild(source);
        }
      } else {
        element.src = urls;
      }
      element.style = 'display: none';
      element.preload = 'auto';
      element.load();
      document.body.appendChild(element);
    }).then(this._bindDoneLoading(key, 'sounds'));
  }

  loadImageAssets(assets) {
    return Promise.all(Object.keys(assets || {}).map(key => this.loadImageAsset(key, assets[key]))).then(() => this);
  }

  loadImageAsset(key, url) {
    if (this.images[key]) {
      return Promise.resolve(this.images[key]);
    }
    this._startLoading(key, 'images');
    const element = document.createElement('IMG');
    element.style.display = 'none';
    return this.images[key] = new Promise((resolve, reject) => {
      element.addEventListener('load', () => resolve(this.images[key] = element));
      element.addEventListener('error', e => reject(e));
      element.src = url;
    }).then(this._bindDoneLoading(key, 'images'));
  }

  loadPrefabAssets(assets) {
    return Promise.all(Object.keys(assets || {}).map(key => this.loadPrefabAsset(key, assets[key]))).then(() => this);
  }

  loadPrefabAsset(key, obj) {
    if (this.prefabs[key]) {
      return Promise.resolve(this.prefabs[key]);
    }
    this._startLoading(key, 'prefabs');
    let importPromise = obj;
    if (obj && obj.defer) {
      importPromise = obj.defer();
    } else if (typeof obj === 'string') {
      importPromise = import(urlToModule(obj));
    }
    return this.prefabs[key] = Promise.resolve(importPromise)
      .then(module => module.default || module)
      .then(prefab => this._resolveDependencies(prefab))
      .then(makePrefabClass)
      .then(this._bindDoneLoading(key, 'prefabs'));
  }

  loadDataAssets(assets) {
    return Promise.all(Object.keys(assets || {}).map(key => this.loadDataAsset(key, assets[key]))).then(() => this);
  }

  loadDataAsset(key, url) {
    if (this.data[key]) {
      return Promise.resolve(this.data[key]);
    }
    this._startLoading(key, 'data');
    return this.data[key] = fetch(url)
      .then(response => response.text())
      .then(this._bindDoneLoading(key, 'data'));
  }

  load(assets) {
    let modules;
    for (const key of Object.keys(assets.prefabs || {})) {
      if (!this.prefabs[key]) {
        this._startLoading(key, 'prefabs');
      }
    }
    return Promise.all([
      this._require(assets.require || []),
      this.loadImageAssets(assets.images),
      this.loadDataAssets(assets.data),
      this.loadSoundAssets(assets.sounds),
    ]).then(([loadedModules]) => {
      modules = loadedModules;
      return this.loadPrefabAssets(assets.prefabs);
    }).then(() => {
      return modules;
    });
  }

  require(...urls) {
    return this._require(urls);
  }

  _require(mods) {
    let isArray = Array.isArray(mods);
    if (isArray) {
      const oMods = {};
      for (const url of mods) {
        oMods[url] = url;
      }
      mods = oMods;
    }
    const keys = Object.keys(mods);
    const promise = Promise.all(keys.map(key => {
      const module = mods[key];
      this._startLoading(key);
      return Promise.resolve(module).then(this._bindDoneLoading(module));
    }));
    if (isArray) {
      return promise;
    }
    return promise.then(modules => {
      const result = {};
      for (let i = 0; i < modules.length; i++) {
        result[keys[i]] = modules[i];
      }
      return result;
    });
  }

  _resolveDependencies(prefab) {
    return this.load(prefab).then(() => prefab);
  }
}

export default AssetStore;
