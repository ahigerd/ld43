let BaseEventTarget = EventTarget;
try {
  new EventTarget();
} catch (e) {
  // MS Edge doesn't support the EventTarget constructor: https://developer.microsoft.com/en-us/microsoft-edge/platform/issues/18274176/
  class EdgeEventTarget {
    constructor() {
      this._eventTarget = document.createElement('DIV');
      for (const method of ['addEventListener', 'removeEventListener', 'dispatchEvent']) {
        this[method] = this._eventTarget[method].bind(this._eventTarget);
      }
    }
  }
  BaseEventTarget = EdgeEventTarget;
}

export default class EventTargetClass extends BaseEventTarget {
  dispatchEvent(eventType, detail = undefined) {
    if (typeof eventType === 'string') {
      super.dispatchEvent(new CustomEvent(eventType, { detail }));
    } else {
      super.dispatchEvent(eventType);
    }
  }
}
