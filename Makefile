dist: dist/main.js

distclean:
	rm -rf dist
	rm -f build/assetlist.js
	rm -f build/assetImports.js

build/node_modules/.bin/webpack:
	cd build
	npm install

dist/main.js: build/node_modules/.bin/webpack FORCE
	cd build && npm run build

clean: build/node_modules/.bin/webpack FORCE
	npm run clean

lint:
	eslint .

FORCE:
