export default {
  images: {
    sprites: 'assets/sprites.png',
  },
  sounds: {
    attack: 'assets/attack.wav',
    damage: 'assets/damage.wav',
    npcdamage: { 'audio/wav': 'assets/npcdamage.wav' },
    pickup: ['assets/pickup.wav'],
    playerdeath: 'assets/playerdeath.wav',
    enemydeath: 'assets/enemydeath.wav',
    npcdeath: 'assets/npcdeath.wav',
  },
  prefabs: {
    hero: 'prefabs/hero.js',
    TileMap: 'prefabs/tilemap.js',
    worshiper: 'prefabs/worshiper.js',
    coin: 'prefabs/coin.js',
    altar: 'prefabs/altar.js',
    mine: 'prefabs/mine.js',
    monster: 'prefabs/monster.js',
    sword: 'prefabs/sword.js',
    danger: 'prefabs/danger.js',
  },
}
