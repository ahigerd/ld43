<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no, user-zoom=fixed" />
<style>
* {
  box-sizing: border-box;
  user-select: none;
  -moz-user-select: none;
  -webkit-user-select: none;
  -ms-user-select: none;
}
#container {
  position: absolute;
}
#container > div {
  border: 1px solid black;
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
}
#camera > canvas {
  width: 100%;
  height: 100%;
}
#container > #statusBar {
  border: none;
  height: 5vh;
}
@media screen and (min-aspect-ratio: 1/1) {
  #container {
    top: 0;
    left: calc(50% - 66.67vh);
    width: 133.33vh;
    height: 100vh;
  }
  .splashText {
    font-size: 4vh;
  }
  .popover {
    line-height: 50vh;
  }
}
@media screen and (max-aspect-ratio: 1/1) {
  #container {
    top: calc(50% - 37.5vw);
    left: 0;
    width: 100vw;
    height: 75vw;
  }
  .splashText {
    font-size: 3vw;
  }
  .popover {
    line-height: 37.5vw;
  }
}
#splash, #helppage {
  background: rgba(255, 255, 255, .5);
  display: none;
  text-align: center;
}
h1 {
  font-weight: bold;
  font-size: 14vmin;
  margin: 5vmin 0 2vmin;
  font-style: italic;
  text-align: center;
}
.splashText {
  display: inline-block;
  width: 95%;
  border: 1px solid black;
  background: rgba(255, 255, 0, .7);
  padding: 2vmin;
  text-align: justify;
}
#helppage h1 {
  font-family: sans-serif;
  font-style: normal;
  font-size:  12vmin;
}
#helppage p {
  margin: .5vh 0 0 0;
}
#loading .animate {
  position: absolute;
  top: calc(50% - 12vmin);
  width: 100%;
  left: 0;
  text-align: center;
  font-size: 20vmin;
  font-weight: bold;
  font-family: sans-serif;
  animation: blink .5s infinite;
}
#progress {
  position: absolute;
  top: calc(50% + 8vmin);
  width: 100%;
  left: 0;
  text-align: center;
  font-size: 5vmin;
  font-family: sans-serif;
}
@keyframes blink {
  0% { color: black; }
  50% { color: transparent; }
};
#camera canvas {
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  image-rendering: pixelated -moz-crisp-edges;
  -webkit-optimize-contrast: pixelated;
}
#pauseContainer {
  position: fixed;
  left: 40%;
  width: 20%;
  overflow: visible;
  bottom: 2vmin;
  text-align: center;
}
#dpad {
  position: fixed;
  background-color: #808080;
  left: 2vmin;
  bottom: 2vmin;
  width: 31vmin;
  height: 31vmin;
  border: 1px solid black;
  border-radius: 7vmin;
}
.dpad div {
  width: 33.5%;
  height: 33.5%;
  line-height: initial;
  text-align: center;
  border: 1px solid black;
  background-color: white;
  color: black;
  margin: -1px;
  box-sizing: content-box;
  border-radius: 5px;
}
.dpad span {
  position: absolute;
  top: calc(50% - 0.5em);
  left: calc(50% - 0.5em);
  height: 1em;
  width: 1em;
  text-align: center;
  display: inline-block;
}
.dpad div.active {
  background-color: black;
  color: white;
}
.dpad .up {
  position: absolute;
  top: 0;
  left: 33.25%;
}
.dpad .left {
  position: absolute;
  left: 0;
  top: 33.25%;
}
.dpad .right {
  position: absolute;
  right: 0;
  top: 33.25%;
}
.dpad .down {
  position: absolute;
  bottom: 0;
  left: 33.25%;
}
#buttons {
  position: fixed;
  right: 2vmin;
  bottom: 2vmin;
  height: 30vmin;
}
.buttons .button, .pause .button {
  display: inline-block;
  text-align: center;
  border: 1px solid black;
  border-radius: 2vmin;
  margin: -1px;
  background-color: white;
  color: black;
}
.buttons .button {
  width: 30vmin;
  height: 30vmin;
  line-height: 30vmin;
}
.pause .button {
  height: 7vmin;
  line-height: 7vmin;
  padding: 0 2vmin;
}
.buttons .button.active, .pause .button.active {
  background-color: black;
  color: white;
}
.bigbutton {
  display: inline-block;
  width: 40%;
  margin: 2vmin 2vw;
  padding: 2vmin 2vw;
  font-size: 6vmin;
  height: 12vmin;
  line-height: 8vmin;
  border: 2px solid black;
  border-radius: 5px;
  font-family: sans-serif;
  color: black;
  text-decoration: none;
}
.popover .bigbutton {
  width: 80%;
}
.bigbutton.start {
  background: linear-gradient(rgb(128, 255, 128), rgb(0, 128, 0));
}
.bigbutton.help {
  background: linear-gradient(rgb(255, 255, 128), rgb(128, 128, 0));
}
.bigbutton.start:active {
  background: linear-gradient(rgb(32, 128, 32), rgb(100, 192, 100));
}
.bigbutton.help:active {
  background: linear-gradient(rgb(128, 128, 32), rgb(220, 220, 0));
}
#helppage td, #helppage th {
  text-align: center;
  padding: .1vmin;
}
#container > .popover {
  background: rgba(255, 255, 128, .9);
  font-family: sans-serif;
  text-align: center;
  display: none;
  position: absolute;
  width: 50%;
  height: 50%;
  border: 1px solid black;
  top: 25%;
  left: 25%;
}
.popover > div {
  display: inline-block;
  vertical-align: middle;
  line-height: initial;
  width: 100%;
}
.popoverContent h2 {
  animation: blink 2s infinite;
}
h2 {
  font-size: 6vmin;
  padding: 0;
  margin: 0 0 4vh;
  font-weight: bold;
}
#statusBar div {
  min-width: 30%;
  margin: 2px;
  border: 1px solid white;
  border-radius: 3px;
  font-family: sans-serif;
  font-size: 3.2vmin;
  padding: 4px;
  color: white;
  background: #0000a0;
  font-weight: bold;
}
#statusBar.onTitle div {
  display: none;
}
#statusBar.onTitle div.onTitle {
  display: block;
}
#statusBar span {
  font-weight: normal;
}
#statusBar div.left {
  float: left;
  text-align: left;
}
#statusBar div.right {
  float: right;
  text-align: right;
}
</style>
</head>
<body>
<div id='fullscreen'>
<div id='container'>
  <div id='camera'></div>
  <div id='loading'>
    <div class='animate'>Loading...</div>
    <div id='progress'>0%</div>
  </div>
  <div id='splash'>
    <h1>Golden Lord</h1>
    <div class='splashText'>
      <center><b>Praise the Golden Lord!</b></center><br/>
      Your people have lived on an island rich in gold and silver for generations, and they love you
      greatly. But now, monsters are invading the island! You have stepped down from heaven to protect
      your worshipers from the invaders, but your divine power depends on the sacrifices of precious
      metal that your people offer up to your altar.
    </div>
    <a id='startButton' class='bigbutton start'>Start Game</a>
    <a id='helpButton' class='bigbutton help'>Help</a>
  </div>
  <div id='helppage'>
    <h1>How to Play</h1>
    <div class='splashText'>
      <table width='100%'>
        <tr><th align='center' width='20%'>Action</th><th align='center' width='30%'>Keyboard</th><th align='center' width='50%'>Touchscreen</th></tr>
        <tr><td>Move</td><td>Arrow Keys</td><td>Virtual D-Pad (left side)</td></tr>
        <tr><td>Attack</td><td>Space Bar</td><td>A Button (right side)</td></tr>
        <tr><td>Pause</td><td>Escape</td><td>Pause Button (center)</td></tr>
      </table>
      <p>Your worshipers will automatically travel between the mines and your altar to collect treasure and sacrifice it to you.</p>
      <p>Sacrifices restore your divine power, automatically allowing you to recover from damage and healing the one to make the offering.</p>
    </div>
    <a id='helpBackButton' class='bigbutton help'>Back</a>
  </div>
  <div id='pauseBox' class='popover'>
    <div>
      <h2>PAUSED</h2>
      <a id='resumeButton' class='bigbutton start'>Resume</a>
    </div>
  </div>
  <div id='gameOver' class='popover'>
    <div>
      <h2>GAME OVER</h2>
      <a id='gameOverStartButton' class='bigbutton start'>Start Over</a>
    </div>
  </div>
  <div id='statusBar' class='onTitle'>
    <div class='left onTitle'>
      High Score: <span id='highScore'>0</span>
    </div>
    <div class='left'>
      Score: <span id='score'>0</span>
    </div>
    <div class='right'>
      Worshipers: <span id='worshipCount'>0</span>
    </div>
  </div>
</div>
<div id='dpad'></div>
<div id='buttons'></div>
<div id='pauseContainer'></div>
</div>
<?php
function loadScript($script, $module = false) {
  echo "<script " . ($module ? 'type="module" ' : '' ) . "src='$script.js?cb=" . filemtime("$script.js") . "'></script>\n";
}
if ($_REQUEST['dev']) {
  loadScript('main', true);
} else {
  loadScript('dist/main', false);
}
?>
</body>
</html>
