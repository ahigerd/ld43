import assets from './scripts/assets';
import GameManger from './scripts/GameManager';

window.assets = assets;

const progress = document.getElementById('progress');
assets.addEventListener('loadprogress', ({ detail: percent}) => progress.innerText = (percent * 100).toFixed(0) + '%'); 
assets.addEventListener('loadcomplete', () => GameManager.init(assets), { once: true });
