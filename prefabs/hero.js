import { Input } from '../Engine';
import Point, { PIXELS_PER_UNIT } from '../Point';
import CharacterCore from '../scripts/CharacterCore';
import assets from '../scripts/assets';

const TAU = Math.PI * 2;

const vectorCache = new Point(0, 0);

function sortObjectsByDepth(lhs, rhs) {
  if (lhs.label == 'sword' && rhs.label != 'sword') return 1;
  if (rhs.label == 'sword' && lhs.label != 'sword') return -1;
  if (lhs.label == 'coin' && lhs.depositing && rhs.label != 'coin') return 1;
  if (rhs.label == 'coin' && rhs.depositing && lhs.label != 'coin') return -1;
  return (lhs.layer - rhs.layer) || (lhs._origin[1] - rhs._origin[1]);
}

export default class Hero extends CharacterCore {
  constructor(origin) {
    super({
      label: 'hero',
      animateHitboxes: false,
      defaultIsAnimating: true,
      defaultAnimationName: 'stand_down',
      hitbox: [-.10, -.05, .10, -.25, 0x1],
      animations: {
        down: [
          [assets.images.sprites, 16, 16, 16, 16],
          [assets.images.sprites, 32, 16, 16, 16],
          [assets.images.sprites, 16, 16, 16, 16],
          [assets.images.sprites, 48, 16, 16, 16],
          250.0,
        ],
        up: [
          [assets.images.sprites, 16, 32, 16, 16],
          [assets.images.sprites, 32, 32, 16, 16],
          [assets.images.sprites, 16, 32, 16, 16],
          [assets.images.sprites, 48, 32, 16, 16],
          250.0,
        ],
        left: [
          [assets.images.sprites, 16, 48, 16, 16],
          [assets.images.sprites, 32, 48, 16, 16],
          [assets.images.sprites, 16, 48, 16, 16],
          [assets.images.sprites, 48, 48, 16, 16],
          250.0,
        ],
        right: [
          [assets.images.sprites, 16, 64, 16, 16],
          [assets.images.sprites, 32, 64, 16, 16],
          [assets.images.sprites, 16, 64, 16, 16],
          [assets.images.sprites, 48, 64, 16, 16],
          250.0,
        ],
        stand_down: [
          [assets.images.sprites, 16, 16, 16, 16],
          250.0,
        ],
        stand_up: [
          [assets.images.sprites, 16, 32, 16, 16],
          250.0,
        ],
        stand_left: [
          [assets.images.sprites, 16, 48, 16, 16],
          250.0,
        ],
        stand_right: [
          [assets.images.sprites, 16, 64, 16, 16],
          250.0,
        ],
        attack: [
          [assets.images.sprites, 48, 112, 16, 16],
          [assets.images.sprites, 64, 112, 16, 16],
          [assets.images.sprites, 64, 112, 16, 16],
          250.0,
        ],
        dead: [
          [assets.images.sprites, 144, 0, 16, 16],
          250.0,
        ],
      },
    }, origin);
  }

  start() {
    this.attackTime = -500;
    super.start();
    this.centerCameraOn(this);
    this.danger = new assets.prefabs.danger(this.origin);
    this.danger.hero = this;
    this.scene.add(this.danger);
  }

  render(camera) {
    super.render(camera);
    if (this.attackTime > 0) {
      const layer = camera.layers[this.layer];
      const pixelRect = this.pixelRect;
      layer.strokeStyle = 'white';
      layer.lineWidth = 3;
      const radius = (750 - this.attackTime) * .0005;
      layer.beginPath();
      layer.arc((pixelRect[0] + pixelRect[2]) * .5 - .5, (pixelRect[1] + pixelRect[3]) * .5, radius * PIXELS_PER_UNIT, 0, TAU, false);
      layer.stroke();
    }
  }

  update(scene, ms) {
    if (this.dead) {
      super.update(scene, ms);
      return;
    }
    this.attackTime -= ms;
    if (this.attackTime > 0) {
      const radius = (750 - this.attackTime) * .0005 + .25;
      for (const monster of this.GameManager.monsters) {
        vectorCache.set(this.origin);
        vectorCache.subtract(monster.origin);
        const mag = vectorCache.magnitude;
        if (mag > radius) continue;
        vectorCache[0] = -vectorCache[0] / mag * (radius - mag);
        vectorCache[1] = -vectorCache[1] / mag * (radius - mag);
        const dir = monster.lastDir;
        monster.moveColliding(500, vectorCache[0], vectorCache[1]);
        monster.lastDir = dir;
        monster.setAnimation('stand_' + dir);
        monster.inflict(10);
        if (monster.weapon) {
          monster.weapon.origin.set(monster.origin);
          switch (dir) {
            case 'right': monster.weapon.origin[0] += .2; break;
            case 'left': monster.weapon.origin[0] -= .2; break;
            case 'down': monster.weapon.origin[1] += .2; break;
            default: monster.weapon.origin[1] -= .2; break;
          }
        }
      }
      return;
    }
    if (Input.keys[' '] && this.attackTime < -500) {
      assets.sounds.attack.play();
      this.playOneShot('attack');
      this.attackTime = 750;
    } else {
      let dx = 0, dy = 0;
      if (Input.keys.ArrowLeft) dx--;
      if (Input.keys.ArrowRight) dx++;
      if (Input.keys.ArrowUp) dy--;
      if (Input.keys.ArrowDown) dy++;
      const moved = this.moveColliding(ms, dx * 1.2, dy * 1.2);
      if (moved) {
        this.centerCameraOn(this);
        this.danger.origin.setXY(this.origin[0], this.origin[1] - .6);
      }
    }
    super.update(scene, ms);
  }

  lateUpdate(scene) {
    scene.objects.sort(sortObjectsByDepth);
  }

  onCollisionEnter(other, coll) {
    this.onCollisionStay(other, coll);
  }

  onCollisionStay(other) {
    if (!this.shouldCollide(other)) {
      return;
    }
    vectorCache.set(this._origin);
    vectorCache.subtract(other._origin);
    vectorCache.normalize();
    this.moveColliding(500, vectorCache[0] * .02, vectorCache[1] * .02);
  }

  onDamage() {
    assets.sounds.damage.play();
  }

  onDeath() {
    assets.sounds.playerdeath.play();
    this.GameManager.gameOver();
    this.hidden = true;
  }
}
