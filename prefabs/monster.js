import assets from '../scripts/assets';
import Point from '../Point';
import Sprite from '../Sprite';
import CharacterCore from '../scripts/CharacterCore';

const PLAYER_HATE_RANGE = 2.5;
const MIN_CHASE_RANGE = .2;
const MAX_ATTACK_RANGE = .4;
const MIN_ATTACK_WAIT = 400;
const ATTACK_WAIT_RANGE = 400;

const vectorCache = new Point(0, 0);

const config = Sprite.prepareConfig({
  label: 'monster',
  animateHitboxes: false,
  defaultIsAnimating: true,
  defaultAnimationName: 'stand_down',
  hitbox: [-.10, -.05, .10, -.25, 0x9],
  animations: {
    down: [
      [assets.images.sprites, 112, 16, 16, 16],
      [assets.images.sprites, 128, 16, 16, 16],
      [assets.images.sprites, 112, 16, 16, 16],
      [assets.images.sprites, 144, 16, 16, 16],
      250.0,
    ],
    up: [
      [assets.images.sprites, 112, 32, 16, 16],
      [assets.images.sprites, 128, 32, 16, 16],
      [assets.images.sprites, 112, 32, 16, 16],
      [assets.images.sprites, 144, 32, 16, 16],
      250.0,
    ],
    left: [
      [assets.images.sprites, 112, 48, 16, 16],
      [assets.images.sprites, 128, 48, 16, 16],
      [assets.images.sprites, 112, 48, 16, 16],
      [assets.images.sprites, 144, 48, 16, 16],
      250.0,
    ],
    right: [
      [assets.images.sprites, 112, 64, 16, 16],
      [assets.images.sprites, 128, 64, 16, 16],
      [assets.images.sprites, 112, 64, 16, 16],
      [assets.images.sprites, 144, 64, 16, 16],
      250.0,
    ],
    stand_down: [
      [assets.images.sprites, 112, 16, 16, 16],
      250.0,
    ],
    stand_up: [
      [assets.images.sprites, 112, 32, 16, 16],
      250.0,
    ],
    stand_left: [
      [assets.images.sprites, 112, 48, 16, 16],
      250.0,
    ],
    stand_right: [
      [assets.images.sprites, 112, 64, 16, 16],
      250.0,
    ],
    attack_down: [
      [assets.images.sprites, 96, 80, 16, 16],
      300.0,
    ],
    attack_up: [
      [assets.images.sprites, 112, 80, 16, 16],
      300.0,
    ],
    attack_left: [
      [assets.images.sprites, 128, 80, 16, 16],
      300.0,
    ],
    attack_right: [
      [assets.images.sprites, 144, 80, 16, 16],
      300.0,
    ],
    dead: [
      [assets.images.sprites, 112, 0, 16, 16],
      250.0,
    ],
  },
});

export default class Monster extends CharacterCore {
  constructor(origin) {
    super(config, origin);
  }

  start() {
    this.attackCooldown = 0;
    super.start();
  }

  update(scene, ms) {
    if (this.dead) {
      super.update(scene, ms);
      return;
    }
    let nearest = this.GameManager.hero;
    const playerDist = nearest.dead ? Infinity : this.origin.distanceTo(nearest.origin);
    let nearestDist = playerDist;
    for (const w of this.GameManager.worshipers) {
      if (w.hidden && w.dead) continue;
      const dist = this.origin.distanceTo(w.origin);
      if (dist < nearestDist) {
        nearest = w;
        nearestDist = dist;
      }
    }
    let attackDist = nearestDist;
    if (playerDist < PLAYER_HATE_RANGE) {
      nearest = this.GameManager.hero;
      nearestDist = playerDist;
    }

    if (!this.oneShotName) {
      if (nearestDist > MIN_CHASE_RANGE) {
        vectorCache.set(nearest.origin);
        vectorCache.subtract(this.origin);
        vectorCache.normalize();
        this.moveColliding(ms, vectorCache[0] * .7, vectorCache[1] * .7);
      }

      if (attackDist < MAX_ATTACK_RANGE && this.attackCooldown <= 0 && this.blinkTimer <= 0) {
        const weapon = this.weapon = new assets.prefabs.sword(this.origin);
        switch (this.lastDir) {
          case 'right': weapon.origin[0] += .2; break;
          case 'left': weapon.origin[0] -= .2; break;
          case 'down': weapon.origin[1] += .2; break;
          default: weapon.origin[1] -= .2; break;
        }
        weapon.setAnimation(this.lastDir);
        scene.add(weapon);
        this.playOneShot('attack_' + this.lastDir);
        this.attackCooldown = MIN_ATTACK_WAIT + Math.random() * ATTACK_WAIT_RANGE;
      }
    }
    if (this.attackCooldown > 0) {
      this.attackCooldown -= ms;
    }
    super.update(scene, ms);
  }

  onCollisionEnter(other, coll) {
    this.onCollisionStay(other, coll);
  }

  onCollisionStay(other) {
    if (!this.shouldCollide(other)) {
      return;
    }
    vectorCache.set(this._origin);
    vectorCache.subtract(other._origin);
    vectorCache.normalize();
    this.moveColliding(500, vectorCache[0] * .02, vectorCache[1] * .02);
  }

  onDeath() {
    assets.sounds.enemydeath.play();
    this.GameManager.addScore(100);
    this.GameManager.kills++;
    if (this.GameManager.kills >= this.GameManager.wave) {
      this.GameManager.wave++;
      this.GameManager.kills = 0;
    }
    this.GameManager.removeMonster(this);
  }

  onDamage() {
    assets.sounds.damage.play();
  }
}
