import assets from '../scripts/assets';
import Point from '../Point';

const vectorCache = new Point(0, 0);

export default {
  label: 'sword',
  animateHitboxes: false,
  defaultIsAnimating: false,
  defaultAnimationName: 'default',
  hitbox: [-.25, 0, .25, -.5, 0x1],
  animations: {
    default: [
      [assets.images.sprites, 0, 80, 16, 16],
      250.0,
    ],
    down: [
      [assets.images.sprites, 96, 96, 16, 16],
      [assets.images.sprites, 96, 112, 16, 16],
      100.0,
    ],
    up: [
      [assets.images.sprites, 112, 96, 16, 16],
      [assets.images.sprites, 112, 112, 16, 16],
      100.0,
    ],
    left: [
      [assets.images.sprites, 128, 96, 16, 16],
      [assets.images.sprites, 128, 112, 16, 16],
      100.0,
    ],
    right: [
      [assets.images.sprites, 144, 96, 16, 16],
      [assets.images.sprites, 144, 112, 16, 16],
      100.0,
    ],
  },

  start() {
    this.timer = 400;
  },

  update(scene, ms) {
    this.timer -= ms;
  },

  lateUpdate(scene) {
    if (this.timer <= 0) {
      this.timer = 0;
      scene.remove(this);
    }
  },

  onCollisionEnter(other) {
    if (other.label == 'hero' || other.label == 'worshiper') {
      if (other.label == 'hero' && other.attackTime > 0 && other.attackTime < 500) return;
      if (other.label == 'worshiper') {
        if (other.mineTimer > 0) return;
        vectorCache.set(other.origin);
        vectorCache.subtract(this.origin);
        vectorCache.normalize();
        other.destination.setXY(other.origin[0] + vectorCache[0] * 2, other.origin[1] + vectorCache[1] * 2);
        other.isWandering = false;
      }
      other.inflict(8);
    }
  },
};
