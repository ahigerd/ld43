import assets from './assets';
import Engine from '../Engine';
import Camera from '../Camera';
import { PIXELS_PER_UNIT } from '../Point';
import Scene from '../Scene';
import TouchControls from '../TouchControls';

let state = 'title';
let score = 0;
let highScore;
try {
  highScore = localStorage.highScore | 0;
} catch (e) {
  highScore = 0;
}
const scoreSpan = document.getElementById('score');
const highScoreSpan = document.getElementById('highScore');
highScoreSpan.innerText = highScore;

function spawnTooClose(x, y, scene) {
  for (const obj of scene.nextObjects) {
    if (obj.isTileMap) continue;
    const dx2 = Math.sqrt(x/PIXELS_PER_UNIT - obj._origin[0]);
    const dy2 = Math.sqrt(y/PIXELS_PER_UNIT - obj._origin[1]);
    if (dx2 + dy2 < 1) return true;
  }
  for (const obj of scene.objects) {
    if (obj.isTileMap) continue;
    const dx2 = Math.sqrt(x/PIXELS_PER_UNIT - obj._origin[0]);
    const dy2 = Math.sqrt(y/PIXELS_PER_UNIT - obj._origin[1]);
    if (dx2 + dy2 < 1) return true;
  }
  return false;
}

const fullscreenEvents = ['fullscreenchange', 'fullscreenerror', 'msfullscreenchange', 'msfullscreenerror', 'webkitfullscreenchange', 'webkitfullscreenerror'];

const GameManager = {
  wave: 0,
  kills: 0,
  domLayers: {},
  engine: new Engine({ showFps: true }),
  camera: new Camera(document.getElementById('camera'), 640, 480),
  disableFullscreen: false,

  init(assets) {
    this.assets = assets;

    this.domLayers = {};
    for (const id of ['loading', 'splash', 'helppage', 'gameOver', 'statusBar', 'worshipCount', 'pauseBox', 'gameOverStartButton', 'startButton', 'helpButton', 'helpBackButton', 'resumeButton', 'fullscreen']) {
      this.domLayers[id] = document.getElementById(id);
    }

    this.bindEvent(this.domLayers.startButton, 'click', this.bindFullscreenThen(this.newLevel));
    this.bindEvent(this.domLayers.helpButton, 'click', this.showHelp);
    this.bindEvent(this.domLayers.helpBackButton, 'click', this.showTitle);
    this.bindEvent(this.domLayers.gameOverStartButton, 'click', this.bindFullscreenThen(this.newLevel));
    this.bindEvent(this.domLayers.resumeButton, 'click', this.bindFullscreenThen(() => this.engine.start()));

    this.camera.setXY(10, 7.5);
    this.camera.setScale(2, 2);
    this.camera.layers[0].font = '16px sans-serif';
    this.camera.layers[0].textAlign = 'center';
    this.camera.layers[0].textBaseline = 'middle';
    this.engine.cameras.push(this.camera);

    this.engine.addEventListener('enginekeydown', e => e.detail.key === 'Escape' && this.engine.pause());
    this.engine.addEventListener('enginepause', this.onPause.bind(this));
    this.bindEvent(this.engine, 'enginestart', this.onStart.bind(this));

    this.touchControls = new TouchControls(
      document.getElementById('dpad'),
      document.getElementById('buttons'),
      document.getElementById('pauseContainer'),
      [
        { label: 'A', key: ' ' },
      ],
    );
    this.touchControls.onPauseClicked = () => state === 'playing' && this.engine.pause();
    this.touchControls.hidden = true;

    GameManager.showTitle();
  },

  bindEvent(target, eventName, callback, options = undefined) {
    target.addEventListener(eventName, (event) => {
      event.preventDefault();
      callback.call(this);
    }, options);
  },

  showTitle() {
    state = 'title';

    this.scene = this.engine.activeScene = new Scene();

    this.tilemap = new assets.prefabs.TileMap([-16, -16]);
    this.scene.add(this.tilemap);

    this.domLayers.loading.style.display = 'none';
    this.domLayers.splash.style.display = 'block';
    this.domLayers.helppage.style.display = 'none';
    this.domLayers.gameOver.style.display = 'none';
    this.domLayers.statusBar.className = 'onTitle';

    this.camera.setXY(10, 7.5);
    // Tick the engine just to trigger a redraw
    this.engine.step();
  },

  showHelp() {
    state = 'help';

    this.domLayers.splash.style.display = 'none';
    this.domLayers.helppage.style.display = 'block';
  },

  bindFullscreenThen(callback) {
    return () => {
      if (this.disableFullscreen) {
        callback.call(this);
        return;
      }
      this.fullscreenCallback = (event) => {
        if (event.type.toLowerCase().includes('error')) {
          this.disableFullscreen = true;
        }
        for (const eventType of fullscreenEvents) {
          fullscreen.removeEventListener(eventType, this.fullscreenCallback);
        }
        this.fullscreenCallback = null;
        callback.call(this);
      };

      const fullscreen = this.domLayers.fullscreen;
      for (const eventType of fullscreenEvents) {
        fullscreen.addEventListener(eventType, this.fullscreenCallback);
      }
      const requestFullscreen = fullscreen.requestFullscreen ||
        fullscreen.webkitRequestFullscreen ||
        fullscreen.msRequestFullscreen ||
        fullscreen.mozRequestFullScreen;
      if (!requestFullscreen) {
        this.disableFullscreen = true;
        callback.call(this);
        return;
      }
      requestFullscreen.call(fullscreen, { navigationUI: 'hide' });
    };
  },

  exitFullscreen() {
    if (!document.fullscreenElement && !document.fullscreen) {
      return false;
    }
    const exitFullscreen = document.exitFullscreen ||
      document.webkitExitFullscreen ||
      document.msExitFullscreen ||
      document.mozExitFullScreen;
    exitFullscreen.call(document);
  },

  newLevel() {
    state = 'playing';
    this.droppedCoins = [];
    this.wave = 2;
    this.kills = 0;

    this.domLayers.splash.style.display = 'none';
    this.domLayers.gameOver.style.display = 'none';
    this.domLayers.statusBar.className = 'inGame';
    this.scene = this.engine.activeScene = new Scene();

    const tilemap = this.tilemap = new assets.prefabs.TileMap([-16, -16]);
    this.scene.add(tilemap);

    this.altar = new assets.prefabs.altar([.25, 0]);
    this.scene.add(this.altar);

    this.hero = new assets.prefabs.hero([.25, 1]);
    this.scene.add(this.hero);

    let x, y;
    this.worshipers = [];
    for (let i = 0; i < 10; i++) {
      const worshiper = new assets.prefabs.worshiper([0, 0]);
      this.worshipers.push(worshiper);
      do {
        x = (Math.random() * 16 + 24) | 0;
        y = (Math.random() * 16 + 24) | 0;
        worshiper._origin.setXY(x / 2 - 16, y / 2 - 16);
      } while (tilemap.bitsAt(worshiper._origin) & 1 ||
        tilemap.bitsAt(worshiper._origin[0], worshiper._origin[1] - 1) & 1 ||
        spawnTooClose(x, y, this.scene));
      this.scene.add(worshiper);
    }
    this.domLayers.worshipCount.innerText = this.worshipers.length;

    this.monsters = [];
    for (let i = 0; i < this.wave; i++) {
      this.spawnMonster();
    }

    this.engine.start();
  },

  onPause() {
    if (state == 'playing') {
      this.exitFullscreen();
      this.domLayers.pauseBox.style.display = 'block';
      this.touchControls.hidden = true;
    }
  },

  onStart() {
    if (state == 'playing') {
      this.domLayers.pauseBox.style.display = 'none';
      if (this.touchControls.touchEnabled) {
        this.touchControls.hidden = false;
      }
    } else {
      this.engine.pause(false);
    }
  },

  gameOver() {
    this.exitFullscreen();
    state = 'gameover';
    this.domLayers.gameOver.style.display = 'block';
  },

  spawnMonster() {
    let x, y;
    const monster = new assets.prefabs.monster([0, 0]);
    this.monsters.push(monster);
    do {
      x = (Math.random() * 64) | 0;
      y = (Math.random() * 64) | 0;
      monster._origin.setXY(x / 2 - 16, y / 2 - 16);
    } while (
      this.camera.aabb.containsXY(x, y) ||
      this.tilemap.bitsAt(monster._origin) & 9 ||
      this.tilemap.bitsAt(monster._origin[0], monster._origin[1] - 1) & 9 ||
      spawnTooClose(x, y, this.scene)
    );
    this.scene.add(monster);
  },

  addScore(points) {
    GameManager.setScore(score + points);
  },

  setScore(points) {
    score = points;
    scoreSpan.innerText = points;
    if (score > highScore) {
      highScore = score;
      highScoreSpan.innerText = highScore;
      try {
        localStorage.highScore = highScore;
      } catch (e) {
        // consume
      }
    }
  },

  removeWorshiper(w) {
    this.worshipers.splice(this.worshipers.indexOf(w), 1);
    this.domLayers.worshipCount.innerText = this.worshipers.length;
  },

  removeMonster(m) {
    this.monsters.splice(this.monsters.indexOf(m), 1);
    while (this.monsters.length < this.wave) {
      this.spawnMonster();
    }
  },
};

window.GameManager = GameManager;

export default GameManager;
