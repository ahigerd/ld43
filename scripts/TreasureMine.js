import GameManager from './GameManager';

export default class TreasureMine {
  constructor() {
    this.mines = [];
  }

  addMine(x, y) {
    const sprite = new GameManager.assets.prefabs.mine(GameManager, [x, y]);
    GameManager.engine.activeScene.add(sprite);
    this.mines.push({ x, y, sprite });
  }
}
