import AssetStore from '../AssetStore';
import assetlist from '../assetlist';

export const assets = new AssetStore(assetlist);
export default assets;
